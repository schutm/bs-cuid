bs-cuid
=======

Synopsis
--------
bs-cuid contains bindings to [cuid](https://github.com/ericelliott/cuid) to be used
with [Bucklescript-TEA](https://github.com/OvermindDL1/bucklescript-tea).

The interface is the same as to the ocaml [cuid](https://github.com/marcoonroad/ocaml-cuid)
packages.


Installation
------------

```
yarn add bs-cuid
```

And don't forget to add it to your bsconfig.json:

```
  "bs-dependencies": [
    "bs-cuid",
    ...
  ],

```


Usage
-----

```
let cuid = Cuid.generate ( )
(* cuid is "c00p6veue0000072slgr067a3", for example *)
```

or

```
let slug = Cuid.slug ( )
(* slug is "u90m0y0m", for example *)
```


Bug tracker
-----------
[Open a new issue](https://gitlab.com/schutm/bs-cuid/issues) for bugs
or feature requests. Please search for existing issues first.

Bugs or feature request will be fixed in the following order, if time
permits:

1. It has a pull-request with a working and tested fix.
2. It is easy to fix and has benefit to myself or a broader audience.
3. It puzzles me and triggers my curiosity to find a way to fix.


Acknowledgements
----------------
Of course this library would be impossible without the excellent
[cuid](https://github.com/ericelliott/cuid) library.


Contributing
------------
Anyone and everyone is welcome to [contribute](CONTRIBUTING.md).


License
-------
This software is licensed under the [ISC License](LICENSE).