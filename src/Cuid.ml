external generate : unit -> string = "generate" [@@bs.module "@schutm/bs-cuid"]
external slug : unit -> string = "slug" [@@bs.module "@schutm/bs-cuid"]

let generate = generate
let slug = slug
