import cuid from 'cuid';

export function generate() {
    return cuid();
}

export function slug() {
    return cuid.slug();
}
